<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';
require_once dirname(__FILE__).'/functions.php';


class LibTimer_Deamon
{

	private static function execCmd($cmd) {
	
		$handle = popen($cmd, 'r');
		if ($handle) {
			$buffer = '';
			while(!feof($handle)) {
			   $buffer .= fgets($handle, 1024);
			}
			pclose($handle);
			return $buffer;
		} 
		
		return false;
	}
	
	
	/**
	 * Get php index file to execute
	 * @return string
	 */
	public function getIndex()
	{
		if (isset($_SERVER['SCRIPT_FILENAME']))
		{
			return $_SERVER['SCRIPT_FILENAME'];
		}
		
		if (isset($_SERVER['DOCUMENT_ROOT']) && isset($_SERVER['PHP_SELF']))
		{
			return $_SERVER['DOCUMENT_ROOT'].$_SERVER['PHP_SELF'];
		}
		
		return 'index.php';
	}



	/**
	 * start php process
	 * @throws ErrorException
	 * @return bool
	 */
	public function start()
	{
		// search for php executable
		
		$php = $this->getBin('php');
		$script = $this->getIndex();
		$command = $php.' '.$script.' "tg=addon/LibTimer/deamon"';
		
		/**
		 * try to prepend nice and ionice if available
		 * @exemple nice -n 19 ionice -c 3 <command>
		 * 
		 * nice :
		 * -20 (most favorable to the process) to 19 (least favorable to the process).
		 * 
		 * ionice :
		 * 0: none, 1: realtime, 2: best-effort, 3: idle
		 */ 
		$registry = bab_getRegistryInstance();
		$registry->changeDirectory('/LibTimer/');
		$n = $registry->getValue('nice');
		$c = $registry->getValue('ionice');
		
		if (null !== $c)
		{
			$ionice = $this->getBin('ionice');
			$command = sprintf('%s -c %d %s', $ionice, $c, $command);
		}
		
		if (null !== $n)
		{
			$nice = $this->getBin('nice');
			$command = sprintf('%s -n %d %s', $nice, $n, $command);
		}
		
		bab_debug($command);
		exec($command.' > /dev/null &');
		
		if (!$this->isStarted())
		{
			throw new ErrorException(libtimer_translate('Error, the deamon has not started'));
			return false;
		}
		
		return true;
	}
	
	/**
	 * 
	 * @param unknown_type $command
	 * @throws ErrorException
	 * @return boolean|string
	 */
	private function getBin($command)
	{
		$arr = explode(' ', self::execCmd('whereis '.$command));
		if (count($arr) < 2) {
			throw new ErrorException(sprintf(libtimer_translate('%s executable not found'), $command));
			return false;
		}
		unset($arr[0]);
		
		return reset($arr);
	}
	
	
	
	/**
	 * 
	 * @return unknown_type
	 */
	public function kill()
	{
		$script = $this->getIndex();
		exec("kill $(ps ax | grep 'php $script tg=addon/LibTimer/deamon' | grep -v grep | awk '{print $1}')");
	}
	
	
	/**
	 * Test if deamon started
	 * @return bool
	 */
	public function isStarted()
	{
		$script = $this->getIndex();
		$ps = self::execCmd("ps ax | grep 'php $script tg=addon/LibTimer/deamon' | grep -v grep");
		return !empty($ps);
	}
	
	/**
	 * Restart deamon automaticaly if stopped and if it has been started manually before
	 * try this for each new user session
	 * @return unknown_type
	 */
	public static function autoRestart()
	{
		
		$D = new LibTimer_Deamon;
		
		if ($D->isStarted())
		{
			return;
		}
		
		try {
			$D->start();
		} catch(ErrorException $e)
		{
			bab_debug($e->getMessage());
		}
		
		
	}
}