<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';


/**
 * 
 */
class LibTimer_Call
{
	private $registry;
	
	const HOUR = 3600;
	
	const DAY = 86400;
	
	const MINUTE = 60;
	
	public function __construct()
	{
		$this->registry = bab_getRegistryInstance();
		$this->registry->changeDirectory('/LibTimer/Events/');
		
	}
	

	
	/**
	 * return duration from last call
	 * @param $event
	 * @return int | null	seconds
	 */
	private function getLastCall(LibTimer_event $event)
	{
		$key = $event->getName();
		$value = $this->registry->getValue($key);
		
		if (null === $value)
		{
			return null;
		}
		
		
		// ISO date time
		
		$ts = bab_mktime($value);
		
		return (time() - $ts);
	}
	
	/**
	 * 
	 * @param LibTimer_event 	$event
	 * @param int				$duration
	 * @return unknown_type
	 */
	private function fireEvent(LibTimer_event $event, $duration)
	{
		$key = $event->getName();
		
		if (null !== $last = $this->getLastCall($event))
		{
			if ($duration > $last)
			{
				return false;
			}
		}
		
		$this->registry->changeDirectory('/LibTimer/Events/');
		$this->registry->setKeyValue($key, date('Y-m-d H:i:s'));
		
		require_once $GLOBALS['babInstallPath'].'utilit/eventincl.php';
		bab_fireEvent($event);
		
		return true;
	}
	
	
	/**
	 * 
	 * @return unknown_type
	 */
	public static function fireAllEvents()
	{
		require_once dirname(__FILE__).'/event.class.php';
		
		$Monthly 	= new LibTimer_eventMonthly;
		$Weekly 	= new LibTimer_eventWeekly;
		$Daily 		= new LibTimer_eventDaily;
		$Hourly 	= new LibTimer_eventHourly;
		$Every30Min	= new LibTimer_eventEvery30Min;
		$Every5Min	= new LibTimer_eventEvery5Min;
		
		$o = new LibTimer_Call;
		$o->fireEvent($Monthly, 	30 * self::DAY);
		$o->fireEvent($Weekly, 		7 * self::DAY);
		$o->fireEvent($Daily, 		self::DAY);
		$o->fireEvent($Hourly, 		self::HOUR);
		$o->fireEvent($Every30Min, 	30 * self::MINUTE);
		$o->fireEvent($Every5Min, 	5 * self::MINUTE);
		
		// update the registry with last call
		
		$registry = bab_getRegistryInstance();
		$registry->changeDirectory('/LibTimer/');
		$registry->setKeyValue('lasthit', date('Y-m-d H:i:s'));
	}
	
	
	
}

	
class Func_Timer extends bab_functionality {
    
    public function getDescription()
    {
        return 'create instance of timer events';
    }
    
	public function newMonthly()
	{
	    require_once dirname(__FILE__).'/event.class.php';
	    return new LibTimer_eventMonthly;
	}
	
	public function newWeekly()
	{
	    require_once dirname(__FILE__).'/event.class.php';
	    return new LibTimer_eventWeekly;
	}
	
	public function newDaily()
	{
	    require_once dirname(__FILE__).'/event.class.php';
	    return new LibTimer_eventDaily;
	}
	
	public function newHourly()
	{
	    require_once dirname(__FILE__).'/event.class.php';
	    return new LibTimer_eventHourly;
	}
	
	public function newEvery30Min()
	{
	    require_once dirname(__FILE__).'/event.class.php';
	    return new LibTimer_eventEvery30Min;
	}
	
	public function newEvery5Min()
	{
	    require_once dirname(__FILE__).'/event.class.php';
	    return new LibTimer_eventEvery5Min;
	}
}