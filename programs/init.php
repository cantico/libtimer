<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';




function LibTimer_upgrade($version_base,$version_ini)
{
    $addon = bab_getAddonInfosInstance('LibTimer');
    
	include_once $GLOBALS['babInstallPath']."utilit/devtools.php";
	

	$addon->removeAllEventListeners();
	$addon->addEventListener('bab_eventPageRefreshed'		, 'LibTimer_onPageRefreshed'	, 'init.php');
	$addon->registerFunctionality('Timer', 'timer.class.php');
	
	$tables = new bab_synchronizeSql($addon->getPhpPath().'dump.sql');
	
	
	
	return true;
}






function LibTimer_onDeleteAddon()
{
	$addon = bab_getAddonInfosInstance('LibTimer');
	$addon->removeAllEventListeners();
	
	$addon->unregisterFunctionality('Timer');
	
	return true;
}


function LibTimer_onPageRefreshed(bab_eventPageRefreshed $event)
{
	$registry = bab_getRegistryInstance();
	$registry->changeDirectory('/LibTimer/');
	$method = $registry->getValue('method', 'page');
	
	if ('page' === $method)
	{
		require_once dirname(__FILE__).'/timer.class.php';
		LibTimer_Call::fireAllEvents();
	}
	
	if ('deamon' === $method)
	{
		$last = $registry->getValue('lastAutoRestart');
		if (null !== $last)
		{
			$ts = bab_mktime($last);
			if ((time() - $ts) < (60*5))
			{
				// dernier auto-restart fait dans les 5 dernieres minutes
				return;
			}
		}
		
		$registry->setKeyValue('lastAutoRestart', date('Y-m-d H:i:s'));
		require_once dirname(__FILE__).'/deamon.class.php';
		LibTimer_Deamon::autoRestart();
	}
}

